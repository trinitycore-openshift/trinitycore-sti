# trinitycore-sti
FROM docker.io/ubuntu:22.04

# TODO: Put the maintainer name in the image metadata
LABEL maintainer="Fulvio Carrus <f.carrus@gmail.com>"

ENV TRINITYCORE_BUILDER_VERSION 1.0

# Set labels used in OpenShift to describe the builder image
LABEL io.k8s.description="Platform for building TrinityCore 3.3.5" \
      io.k8s.display-name="TrinityCore 3.3.5" \
      io.openshift.tags="builder,trinitycore" \
      io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

RUN apt-get update && \
    apt-get -y install \
	curl p7zip git clang cmake make gcc g++ \
	libmysqlclient-dev libssl-dev \
	libbz2-dev libreadline-dev libncurses-dev \
	libboost-all-dev && \
    update-alternatives --install /usr/bin/cc cc /usr/bin/clang 100 && \
    update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang 100 && \
    apt-get clean

COPY ./s2i/bin/ /usr/libexec/s2i
RUN chmod -R a+x /usr/libexec/s2i

ENV APP_ROOT=/opt/trinity

RUN mkdir -p ${APP_ROOT}/bin ${APP_ROOT}/etc ${APP_ROOT}/data ${APP_ROOT} && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R ug+rwx ${APP_ROOT}

RUN git config --global --add safe.directory '*'

USER 1001
WORKDIR ${APP_ROOT}

CMD ["/usr/libexec/s2i/usage"]
